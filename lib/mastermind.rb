class Code

  PEGS = {"r" => "Red",
          'g' => "Green",
          'b' => "Blue",
          'y' => "Yellow",
          'o' => "Orange",
          'p' => "Purple"}
          
  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
    @pegs[idx]
  end

  attr_reader :pegs

  def self.parse(str)
    @arr = str.downcase.split('')
    raise if @arr.any?{|c| !PEGS.keys.include?(c)}
    self.new(@arr)
  end

  def self.random
    @sample = []; 4.times{@sample << PEGS.keys.sample()}
    self.new(@sample)
  end

  def exact_matches(guess) #guess.class = Code
    (0..@pegs.length-1).reduce(0) do |acc, el|
      acc += 1 if @pegs[el] == guess.pegs[el]
      acc
    end
  end

  def near_matches(guess)
    @guess = guess.pegs
    @counter = 0

    @pegs.each_with_index do |el,i|
      if @guess.include?(el)
        @counter += 1
        @guess = @guess.rotate(@guess.index(el))
        @guess.shift
      end
    end
    @counter - exact_matches(guess)
  end

  def ==(code)
    return false if code.class != Code
    @pegs == code.pegs
  end
end

class Game

  def initialize(code=Code.random)
    @secret_code = code
  end

  def get_guess
    Code.random
  end

  def display_matches(get_guess)
    puts "exact"
    puts "near"
  end

  attr_reader :secret_code

end
